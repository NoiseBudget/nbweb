import os
import h5py
import numpy as np


def info_from_file(path):
    """NB ifo/date from file name

    Assume data file name:

      NB_<IFO>_<DOF>_<DATE>.<ext>

    """
    name = os.path.basename(os.path.splitext(path)[0]).split('_')
    ifo = name[1]
    dof = name[2]
    date = name[3]
    return ifo, dof, date


class Trace(object):
    def __init__(self, data, attrs={}):
        self.data = data
        self.attrs = {}
        self.attrs.update(attrs)

    @property
    def tags(self):
        if 'tags' in self.attrs:
            return set(self.attrs['tags'].split(' '))
        else:
            return set()


class Budget(object):
    def __init__(self, freq, traces, xunit='Frequency [Hz]',
                 yunit='Displacement [m/%sHz]' % u'\u221a',
                 sum=None, measured=None, references={}):
        self.freq = freq
        self.traces = traces
        for label, trace in self.traces.items():
            if not (isinstance(trace, Trace) or isinstance(trace, Budget)):
                self.traces[label] = Trace(trace)
        if sum is not None:
            self.traces['Sum'] = Trace(sum, attrs={'tags': 'sum'})
        if measured is not None:
            self.traces['Measured'] = Trace(measured,
                                            attrs={'tags': 'measured'})
        if references:
            for ref in references:
                self.traces[ref] = Trace(references[ref],
                                         attrs={'tags': 'reference'})
        self.attrs = {}
        self.attrs['xunit'] = xunit
        self.attrs['yunit'] = yunit

    @classmethod
    def from_hdf5(cls, path, h5object='traces'):
        with h5py.File(path, 'r') as f:
            try:
                if f.attrs['schema_version'] > 1:
                    return Budget.from_hdf5v2(path, h5object=h5object)
                else:
                    return Budget.from_hdf5v1(path)
            except KeyError:
                # some older files had 'version' instead of 'schema_version'
                return Budget.from_hdf5v1(path)

    @classmethod
    def from_hdf5v2(cls, path, h5object='traces'):
        # this function is called recursively to handle subbudgets
        with h5py.File(path, 'r') as f:
            freq = np.copy(f.get('frequency')).flatten()
            nb = f.get(h5object)
            traces = {}
            for l, d in nb.iteritems():
                if hasattr(d, 'keys'):
                    subbudget = h5object + '/' + l
                    traces[l] = Budget.from_hdf5(path, h5object=subbudget)
                else:
                    traces[l] = Trace(np.copy(d).flatten())
                    traces[l].attrs.update(d.attrs)

            ifo, dof, date = info_from_file(path)
            subs = h5object[len('traces'):].split('/')
            dof = ' '.join([dof] + subs)
            attrs = {'name': ' '.join([ifo, dof, date]), 'date': date}
            attrs.update(nb.attrs)

        o = cls(freq, traces)
        o.attrs.update(attrs)
        return o

    @classmethod
    def from_hdf5v1(cls, path):
        with h5py.File(path, 'r') as f:
            nb = f.get('/')
            freq = np.copy(nb['frequency']).flatten()
            traces = {l: np.copy(d).flatten()
                      for l, d in nb['traces'].iteritems()}
            sum = np.copy(nb['sum']).flatten()
            try:
                measured = np.copy(nb['measured']).flatten()
            except KeyError:
                measured = None

            ifo, dof, date = info_from_file(path)
            attrs = {}
            attrs.update(f.attrs)
            attrs.update({'name': ' '.join([ifo, dof, date]), 'date': date})

        o = cls(freq, traces, sum=sum, measured=measured)
        o.attrs.update(attrs)
        return o

    def to_hdf5(self, path):
        with h5py.File(path, 'w') as f:
            f.create_dataset('frequency', data=self.freq)

            def write_tracegroup(nb, h5object='traces'):
                # this function is called recursively to handle subbudgets
                g = f.create_group(h5object)
                g.attrs.update(nb.attrs)
                for l, d in nb.traces.iteritems():
                    if isinstance(d, Budget):
                        subbudget = h5object + '/' + l
                        write_tracegroup(d, h5object=subbudget)
                    else:
                        ds = f.create_dataset(h5object + '/' + l, data=d.data)
                        ds.attrs.update(d.attrs)

            write_tracegroup(self)
            f.attrs['schema'] = 'LIGO Noise Budget'
            f.attrs['schema_version'] = 2

    @property
    def tags(self):
        if 'tags' in self.attrs:
            return set(self.attrs['tags'].split(' '))
        else:
            return set()

    @property
    def data(self):
        d = [t.data for t in self.traces.values() if 'data' in t.tags]
        if d:
            return d[0]
        d = [t.data for t in self.traces.values() if 'measured' in t.tags]
        if d:
            return d[0]
        d = [t.data for t in self.traces.values() if 'sum' in t.tags]
        if d:
            return d[0]
