import os
import matplotlib
matplotlib.use('Agg')  # non-interactive backend
import matplotlib.pyplot as plt
import mpld3
from mpld3 import plugins

from . import const
from .. import budget

tooltip_css = """
table
{
  border-collapse: collapse;
}
th
{
  color: #ffffff;
  background-color: #000000;
}
td
{
  background-color: #cccccc;
}
table, th, td
{
  font-family:Arial, Helvetica, sans-serif;
  border: 1px solid black;
  text-align: right;
}
"""


def plot(source, xlim=const.XLIM, ylim=const.YLIM):

    # fig, ax = plt.subplots()
    fig = plt.figure(figsize=(10, 6))
    gs = plt.GridSpec(1, 1)
    gs.update(left=0.1, right=0.8, bottom=0.1, top=0.9)
    ax = plt.subplot(gs[0, 0])

    ax.grid(True, alpha=0.3)

    for label, x, y, color, alpha, linewidth in source['traces']:
        ax.loglog(x, y, label=label, color=color, alpha=alpha,
                  linewidth=linewidth)

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    ax.set_xlabel(source['xlabel'])
    ax.set_ylabel(source['ylabel'])
    ax.set_title(source['title'])

    # fancy legend
    handles, labels = ax.get_legend_handles_labels()
    interactive_legend = plugins.InteractiveLegendPlugin(
        zip(handles, ax.lines),
        labels,
        alpha_unsel=0.3,
        alpha_over=1.5,
        start_visible=True)
    plugins.connect(fig, interactive_legend)

    # mouse over tooltip
    # plugins.connect(fig, LineLabelTooltip(lines[0]))
    # tooltip = plugins.PointHTMLTooltip(
    #     points[0], labels,
    #     voffset=10, hoffset=10, css=tooltip_css)
    # plugins.connect(fig, tooltip)

    return fig


def write_budget_plot(nb, path, urlroot, urlbasepath,
                      xlim=const.XLIM, ylim=const.YLIM):
    source = {'title': nb.attrs['name'],
              'xlabel': nb.attrs['xunit'],
              'ylabel': nb.attrs['yunit'],
              'traces': []}
    for i, (label, trace) in enumerate(nb.traces.items()):
        # traceinfo: label, x, y, color, alpha, linewidth
        traceinfo = [label, nb.freq, trace.data]
        if 'sum' in trace.tags:
            traceinfo += ['black', 1.0, 3]
        elif 'measured' in trace.tags:
            traceinfo += [const.COLORS[-1], 1.0, 2]
        else:
            traceinfo += [const.COLORS[i % len(const.COLORS)], 0.4, 2]
        source['traces'] += [traceinfo]

    basepath = os.path.splitext(path)[0]
    file_html = basepath + '.mpld3.html'
    fig = plot(source, xlim=const.XLIM, ylim=const.YLIM)
    mpld3.save_html(fig, file_html)


def write_budget_html(template, root, urlroot, basepath, **kwargs):
    fbpath = os.path.join(root, basepath)
    with open(fbpath+'.html', 'w') as fh:
        with open(fbpath+'.mpld3.html', 'r') as ft:
            body = ft.read()
        fh.write(template.render(
            urlroot=urlroot,
            baseurl=os.path.join(urlroot, basepath),
            basename=os.path.basename(basepath),
            body=body,
            **kwargs
            ))
        fh.write('\n')


def write_front_html(template, outpath, root, urlroot, basepaths, **kwargs):
    ifos = []
    for basepath in basepaths:
        fbpath = os.path.join(root, basepath)

        with open(fbpath+'.mpld3.html', 'r') as ft:
            body = ft.read()

        ifo, dof, date = budget.info_from_file(basepath)
        title = ' '.join([ifo, dof, date])
        url = os.path.join(urlroot, basepath) + '.html'

        ifos.append((ifo, title, url, body))

    with open(outpath, 'w') as f:
        f.write(template.render(
            urlroot=urlroot,
            ifos=ifos,
            **kwargs
            ).encode('utf8'))


def write_noise_plot(dof, label, noiselist, urlroot, path,
                     xlim=const.XLIM, ylim=const.YLIM,
                     x_axis_label='', y_axis_label=''):
    source = {'title': '{dof} {label}'.format(dof=dof, label=label),
              'xlabel': x_axis_label, 'ylabel': y_axis_label,
              'traces': []}
    for i, (budget_basepath, x, y) in enumerate(noiselist):
        ifo, dof, date = budget.info_from_file(budget_basepath)
        label = ifo + ' ' + date
        # traceinfo: label, x, y, color, alpha, linewidth
        traceinfo = [label, x, y, const.COLORS[i % len(const.COLORS)], 0.4, 2]
        source['traces'] += [traceinfo]

    basepath = os.path.splitext(path)[0]
    file_html = basepath + '.mpld3.html'
    fig = plot(source, xlim=const.XLIM, ylim=const.YLIM)
    mpld3.save_html(fig, file_html)


def write_noise_html(template, root, urlroot, basepath, dof, label, **kwargs):
    fbpath = os.path.join(root, basepath)
    with open(fbpath+'.html', 'w') as fh:
        with open(fbpath+'.mpld3.html', 'r') as ft:
            body = ft.read()
        fh.write(template.render(
            urlroot=urlroot,
            baseurl=os.path.join(urlroot, basepath),
            basename=os.path.basename(basepath),
            dof=dof,
            label=label,
            body=body,
            **kwargs
            ).encode('utf8'))
        fh.write('\n')
