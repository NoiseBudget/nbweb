#!/usr/bin/env python

from __future__ import division
import os
import subprocess
import bokeh.plotting as plt
from bokeh.models import HoverTool, FuncTickFormatter
from bokeh.colors import RGB
from bokeh.embed import components
from bokeh.resources import CDN
from bokeh.io import export_svgs
from selenium import webdriver

from . import const
from .. import budget
from . import util

COLORS = [RGB(int(255*r), int(255*g), int(255*b)) for r, g, b in const.COLORS]


def plot(source, **kwargs):
    hover = HoverTool(
        tooltips=[
            ("trace", "@labels"),
            ("x", "$x"),
            ("y", "$y"),
        ]
    )

    TOOLS = ['pan', 'box_zoom', 'wheel_zoom',
             'crosshair',
             hover,
             'tap',
             'reset', 'save',
             ]

    fig = plt.figure(
        plot_width=1000, plot_height=600,
        tools=TOOLS,
        x_axis_type='log',
        y_axis_type='log',
        **kwargs
    )

    # FIXME: multi_line does not yet support vectorized line_dash
    fig.multi_line(source=source, xs='xbin', ys='ybin', legend='labels',
                   line_color='line_colors', line_alpha='line_alphas',
                   line_width='line_widths', line_dash='solid',
                   hover_line_color='line_colors', hover_line_alpha=1.0,
                   line_join='bevel')

    # FIXME: unicode hack to render superscripts in tick labels
    # https://github.com/bokeh/bokeh/issues/6031
    # font testing: https://codepen.io/anon/pen/vamGNg
    logexpformatter = FuncTickFormatter(code="""
      var expon = Math.log(tick)/Math.LN10
      // in case of small exponent use simple formatting
      if (Math.abs(expon) < 3.01)
      {
        return tick.toFixed(8).replace(/\.?0*$/,'');
      }
      // in case of fractional exponent (deep zoom) use simple formatting
      if (Math.abs(expon - Math.round(expon)) > 1e-8)
      {
        return tick.toExponential(8).replace(/\.?0*e/,'e');
      }
      // unicode log tick formatting
      var str = Math.round(expon).toString();
      var newStr = "";
      for (var i = 0; i < str.length; i++)
      {
        var code = str.charCodeAt(i);
        switch(code) {
        case 45: // "-"
          newStr += String.fromCharCode(8315);
          break;
        case 46: // "."
          newStr += String.fromCharCode(183);
          break;
        case 49: // "1"
          newStr += String.fromCharCode(185);
          break;
        case 50: // "2"
          newStr += String.fromCharCode(178);
          break;
        case 51: // "3"
          newStr += String.fromCharCode(179);
          break;
        default: // "4"-"9", "0"
          newStr += String.fromCharCode(code+8256)
        }
      }
    return "10" + newStr;""")

    fig.xaxis[0].formatter = logexpformatter
    fig.yaxis[0].formatter = logexpformatter
    fig.xaxis[0].axis_label_text_font = const.UNICODEFONT
    fig.xaxis[0].axis_label_text_font_style = 'normal'
    fig.xaxis[0].axis_label_text_font_size = const.FONTSIZE
    fig.xaxis[0].major_label_text_font = const.UNICODEFONT
    fig.xaxis[0].major_label_text_font_size = const.FONTSIZE
    fig.yaxis[0].axis_label_text_font = const.UNICODEFONT
    fig.yaxis[0].axis_label_text_font_style = 'normal'
    fig.yaxis[0].axis_label_text_font_size = const.FONTSIZE
    fig.yaxis[0].major_label_text_font = const.UNICODEFONT
    fig.yaxis[0].major_label_text_font_size = const.FONTSIZE
    fig.title.text_font = const.UNICODEFONT
    fig.title.text_font_size = const.FONTSIZE
    fig.legend.label_text_font = const.UNICODEFONT
    fig.legend.label_text_font_size = const.FONTSIZE

    return fig


def write_plot(fig, path):
    file_js = path + '.js'
    file_div = path + '.div'
    file_svg = path + '.svg'
    file_pdf = path + '.pdf'
    js, div = components(fig)
    with open(file_js, 'w') as f:
        f.write(js)
    with open(file_div, 'w') as f:
        f.write(div)
    fig.output_backend = 'svg'
    chrome_options = webdriver.ChromeOptions()
    chrome_options.headless = True
    chrome_options.add_argument('--no-sandbox')
    chrome = webdriver.Chrome(chrome_options=chrome_options)
    export_svgs(fig, file_svg, webdriver=chrome)
    subprocess.check_call(['inkscape', file_svg, '--export-pdf=' + file_pdf])


def write_budget_plot(nb, path, urlroot, urlbasepath,
                      xlim=const.XLIM, ylim=const.YLIM):
    bin_ = ((len(nb.freq) - 1) // const.MAXPOINTS) + 1
    binfunc = util.linear_bin
    binspec = ' [bin {n}]'.format(n=bin_)
    # use log_bin when points are linearly spaced
    if ((nb.freq[1]-nb.freq[0])/(nb.freq[-1]-nb.freq[-2]) - 1) < 1e-8:
        binfunc = util.log_bin
        binspec = ' [bin {n}L]'.format(n=bin_)

    title = nb.attrs['name']
    if bin_ > 1:
        title += binspec

    # assemble traces
    # xs/ys - placeholders for full data, for future dynamic rebinning
    xs = []
    ys = []
    xbin = []
    ybin = []
    labels = []
    line_colors = []
    line_alphas = []
    line_widths = []

    for i, (label, trace) in enumerate(nb.traces.items()):
        data = trace.data
        xs += [[]]  # [nb.freq]
        ys += [[]]  # [data]
        xbin += [binfunc(nb.freq, bin_)]
        ybin += [binfunc(data, bin_)]
        labels += [label]
        if 'sum' in trace.tags:
            line_colors += [RGB(0, 0, 0)]
            line_alphas += [1.0]
            line_widths += [3]
        elif 'measured' in trace.tags:
            line_colors += [COLORS[-1]]
            line_alphas += [1.0]
            line_widths += [2]
        else:
            line_colors += [COLORS[i % len(COLORS)]]
            line_alphas += [0.4]
            line_widths += [2]

    source = plt.ColumnDataSource(dict(xs=xs, ys=ys, xbin=xbin, ybin=ybin,
                                       labels=labels, line_colors=line_colors,
                                       line_alphas=line_alphas,
                                       line_widths=line_widths))

    fig = plot(source, title=title, x_range=xlim, y_range=ylim,
               x_axis_label=nb.attrs['xunit'], y_axis_label=nb.attrs['yunit'])
    path = os.path.splitext(path)[0]
    write_plot(fig, path)


def write_budget_html(template, root, urlroot, basepath, **kwargs):
    fbpath = os.path.join(root, basepath)
    with open(fbpath+'.js', 'r') as ft:
        script = ft.read()
    script += CDN.render_js() + CDN.render_css()
    with open(fbpath+'.div', 'r') as ft:
        body = ft.read()
    with open(fbpath+'.html', 'w') as fh:
        fh.write(template.render(
            urlroot=urlroot,
            baseurl=os.path.join(urlroot, basepath),
            basename=os.path.basename(basepath),
            body=body,
            script=script,
            **kwargs
            ).encode('utf8'))
        fh.write('\n')


def write_front_html(template, outpath, root, urlroot, basepaths, **kwargs):
    ifos = []
    script = ''
    for basepath in basepaths:
        fbpath = os.path.join(root, basepath)

        with open(fbpath+'.js', 'r') as f:
            script += f.read()

        ifo, dof, date = budget.info_from_file(basepath)
        title = ' '.join([ifo, dof, date])
        url = os.path.join(urlroot, basepath) + '.html'
        with open(fbpath+'.div', 'r') as f:
            body = f.read()

        ifos.append((ifo, title, url, body))

    script += CDN.render_js() + CDN.render_css()

    with open(outpath, 'w') as f:
        f.write(template.render(
            urlroot=urlroot,
            ifos=ifos,
            script=script,
            **kwargs
            ).encode('utf8'))


def write_noise_plot(dof, label, noiselist, urlroot, path,
                     xlim=const.XLIM, ylim=const.YLIM, **kwargs):
    title = '{dof} {label}'.format(dof=dof, label=label)

    # assemble traces
    # xs/ys - placeholders for full data, for future dynamic rebinning
    xs = []
    ys = []
    xbin = []
    ybin = []
    labels = []
    line_colors = []
    line_alphas = []
    line_widths = []
    for i, n in enumerate(noiselist):
        budget_basepath, x, y = n
        ifo, dof, date = budget.info_from_file(budget_basepath)
        label = ifo + ' ' + date

        bin_ = ((len(x) - 1) // const.MAXPOINTS) + 1
        binfunc = util.linear_bin
        binspec = ' [bin {n}]'.format(n=bin_)
        # use log_bin when points are linearly spaced
        if ((x[1]-x[0])/(x[-1]-x[-2]) - 1) < 1e-8:
            binfunc = util.log_bin
            binspec = ' [bin {n}L]'.format(n=bin_)
        if bin_ > 1:
            label += binspec

        xs += [[]]  # [x]
        ys += [[]]  # [y]
        xbin += [binfunc(x, bin_)]
        ybin += [binfunc(y, bin_)]
        labels += [label]
        line_colors += [COLORS[i % len(COLORS)]]
        line_alphas += [0.4]
        line_widths += [2]

    source = plt.ColumnDataSource(dict(xs=xs, ys=ys, xbin=xbin, ybin=ybin,
                                       labels=labels, line_colors=line_colors,
                                       line_alphas=line_alphas,
                                       line_widths=line_widths))

    fig = plot(source, title=title, x_range=xlim, y_range=ylim,
               **kwargs)
    write_plot(fig, path)


def write_noise_html(template, root, urlroot, basepath, dof, label, **kwargs):
    fbpath = os.path.join(root, basepath)
    with open(fbpath+'.js', 'r') as ft:
        script = ft.read()
    script += CDN.render_js() + CDN.render_css()
    with open(fbpath+'.div', 'r') as ft:
        body = ft.read()
    with open(fbpath+'.html', 'w') as fh:
        fh.write(template.render(
            urlroot=urlroot,
            baseurl=os.path.join(urlroot, basepath),
            basename=os.path.basename(basepath),
            dof=dof,
            label=label,
            body=body,
            script=script,
            **kwargs
            ).encode('utf8'))
        fh.write('\n')
    pass
