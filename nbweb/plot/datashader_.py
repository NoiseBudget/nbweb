#!/usr/bin/env python

import os
import bokeh.plotting as plt
from bokeh.embed import components
from bokeh.resources import CDN
from bokeh.colors import RGB
from bokeh.models import Legend
import pandas as pd
import datashader as ds
import datashader.transfer_functions as tf

from . import const
from .. import budget

COLORS = [RGB(int(255*r), int(255*g), int(255*b)) for r, g, b in const.COLORS]


def write_plot(nb, path, urlroot, urlbasepath,
               xlim=const.XLIM, ylim=const.YLIM):
    # create a new plot
    fig = plt.figure(
        plot_width=1000, plot_height=600,
        # tools=TOOLS,
        title=nb.title,
        x_axis_type='log', x_range=xlim,
        y_axis_type='log', y_range=ylim,
        x_axis_label=nb.xunit,
        y_axis_label=nb.yunit,
        # output_backend='webgl',
    )

    # sum
    ys = [nb.sum]
    labels = ['Sum']
    line_colors = [RGB(0, 0, 0)]
    line_alphas = [1.0]

    # traces
    for i, t in enumerate(nb.traces):
        label, data = t
        ys += [data]
        labels += [label]
        line_colors += [COLORS[i % len(COLORS)]]
        line_alphas += [0.1]

    # render datashaded curve images
    source = pd.DataFrame(dict([('Freq', nb.freq)] +
                               [(label, y) for label, y in zip(labels, ys)]))

    cvs = ds.Canvas(x_range=xlim, y_range=ylim,
                    plot_height=fig.plot_height*2, plot_width=fig.plot_width*2,
                    x_axis_type='log', y_axis_type='log')
    aggs = [cvs.line(source, 'Freq', c,
                     agg=ds.count()).rename({c: 'value'}) for c in labels]
    imgs = [tf.dynspread(tf.shade(agg, cmap=[color.to_hex()],
                                  alpha=[int(255*alpha)]),
                         max_px=3, threshold=1)
            for agg, color, alpha in zip(aggs, line_colors, line_alphas)]

    # write curve images to files
    filebasepath = os.path.splitext(path)[0]
    for n, img in enumerate(imgs):
        file_png = '{basename}_{n}.png'.format(basename=filebasepath, n=n)
        img.to_pil().save(file_png)

    # add curve images and clickable legend to plot
    renderers = []
    for n, linedata in enumerate(zip(imgs, line_colors, line_alphas)):
        img, line_color, line_alpha = linedata
        url = '{urlroot}/{urlbasename}_{n}.png'.format(
            urlroot=urlroot, urlbasename=urlbasepath, n=n
        )
        img_renderer = fig.image_url([url], xlim[0], ylim[0],
                                     xlim[1]-xlim[0], ylim[1]-ylim[0],
                                     anchor='bottom_left')
        # FIXME: alpha mismatch in bokeh vs datashader
        line_renderer = fig.line([], [], line_color=line_color,
                                 line_alpha=line_alpha**(1./3), line_width=2)
        renderers += [[line_renderer, img_renderer]]

    legend = Legend(items=zip(labels, renderers), location='top_right',
                    orientation='vertical', click_policy='hide')
    fig.add_layout(legend, 'right')

    # write output to files
    file_js = filebasepath + '.js'
    file_div = filebasepath + '.div'
    js, div = components(fig)
    with open(file_js, 'w') as f:
        f.write(js)
    with open(file_div, 'w') as f:
        f.write(div)


def write_html(template, root, urlroot, basepath, **kwargs):
    fbpath = os.path.join(root, basepath)
    with open(fbpath+'.js', 'r') as ft:
        script = ft.read()
    head = CDN.render_js() + CDN.render_css() + script
    with open(fbpath+'.div', 'r') as ft:
        body = ft.read()
    with open(fbpath+'.html', 'w') as fh:
        fh.write(template.render(
            urlroot=urlroot,
            baseurl=os.path.join(urlroot, basepath),
            basename=os.path.basename(basepath),
            head=head,
            body=body,
            **kwargs
            ).encode('utf8'))
        fh.write('\n')


def write_front_html(template, outpath, root, urlroot, basepaths, **kwargs):
    head = CDN.render_js() + CDN.render_css()

    ifos = []
    for basepath in basepaths:
        fbpath = os.path.join(root, basepath)

        with open(fbpath+'.js', 'r') as f:
            script = f.read()
        head += script

        title = budget.title_from_file(basepath)
        url = os.path.join(urlroot, basepath) + '.html'
        with open(fbpath+'.div', 'r') as f:
            body = f.read()

        ifos.append((title, url, body))

    with open(outpath, 'w') as f:
        f.write(template.render(
            urlroot=urlroot,
            head=head,
            ifos=ifos,
            **kwargs
            ).encode('utf8'))
