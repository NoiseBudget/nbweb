#!/usr/bin/env python

from __future__ import division
from numpy import log, log10, exp, ceil, floor, zeros, isfinite


def autorange_budget(nb):
    xmin = min(nb.freq[nb.freq > 0])
    xmax = max(nb.freq)
    xlim = [0.99*10**floor(log10(xmin)), 1.01*10**ceil(log10(xmax))]

    tracemin = [min(t.data[isfinite(t.data)]) for t in nb.traces.values()
                if t.data.any() and isfinite(t.data).any()]
    tracemax = [max(t.data[isfinite(t.data)]) for t in nb.traces.values()
                if t.data.any() and isfinite(t.data).any()]
    sumdata = [t.data for t in nb.traces.values() if 'sum' in t.tags][0]
    summin = min(sumdata[isfinite(sumdata)])
    summax = max(sumdata[isfinite(sumdata)])
    minY = min([min(tracemax), summin])
    maxY = max([max(tracemin), summax])
    ylim = [0.99*10**floor(log10(minY)), 1.01*10**ceil(log10(maxY))]

    return xlim, ylim


def autorange_noises(noiselist):
    xmin = min(min(x[x > 0]) for bp, x, y in noiselist)
    xmax = max(max(x) for bp, x, y in noiselist)
    xlim = [0.99*10**floor(log10(xmin)), 1.01*10**ceil(log10(xmax))]

    ymin = [min(y[isfinite(y)]) for bp, x, y in noiselist
            if y.any() and isfinite(y).any()]
    ymax = [max(y[isfinite(y)]) for bp, x, y in noiselist
            if y.any() and isfinite(y).any()]
    minY = min([min(ymax), max(ymin)])
    maxY = max([max(ymin), min(ymax)])
    ylim = [0.99*10**floor(log10(minY)), 1.01*10**ceil(log10(maxY))]

    return xlim, ylim


def linear_bin(data, bin_):
    if bin <= 1:
        return data

    nBins = len(data)//bin_
    return data[:bin_*nBins].reshape((nBins, bin_)).mean(axis=1)


# adapted from gdssigproc.c
def log_bin(data, bin_):
    if bin_ <= 1:
        return data

    dlen = len(data)
    nBins = dlen//bin_
    dx = exp(log(dlen) * bin_ / dlen)
    x1 = 1.

    out = zeros(nBins, dtype=data.dtype)
    for n in range(nBins):
        j1 = int(x1)
        x2 = x1 * dx
        j2 = int(min(ceil(x2), dlen))
        y = data[j1-1]/2 - (x1-j1) * \
            (data[j1-1] + (x1-j1) * (data[j1] - data[j1-1])/2)
        for j in range(j1+1, j2):
            y += data[j-1]
        y += data[j2-1]/2 - (j2-x2) * \
            (data[j2-1] - (j2-x2) * (data[j2-1] - data[j2-2])/2)
        out[n] = y / (x2-x1)
        x1 = x2

    return out
