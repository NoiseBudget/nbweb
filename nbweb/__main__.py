from __future__ import division, print_function
import os
import re
import argparse
import importlib
import subprocess

import jinja2

from . import budget
from .plot import util

##########

DEFAULT_ROOT = os.path.expanduser('~/public_html')
DEFAULT_URL = 'https://ldas-jobs.ligo.caltech.edu/~noisebudget'
DEFAULT_DATAURL = 'https://git.ligo.org/NoiseBudget/NBdata_{ifo}.git'
DEFAULT_SUMMARYURL = 'https://ldas-jobs.ligo.caltech.edu/~detchar/summary/day/'

##########


def find_all(root, pat='.*\.hdf5$', striproot=False):
    prog = re.compile(pat)
    for (dirpath, dirnames, filenames) in os.walk(root):
        if striproot:
            d = len(root) + 1
            dirpath = dirpath[d:]
        for f in filenames:
            if prog.match(f):
                yield os.path.join(dirpath, f)


def gen_nav(root, prefix='', depth=2):
    nav = {}
    for path in find_all(root, striproot=True):
        n = nav
        p = path.split('/')
        for d in p[depth:-2]:
            n = n.setdefault(d, {})
        base = os.path.splitext(p[-1])[0]
        name = base
        link = os.path.join(prefix, p[-2], base) + '.html'
        e = (name, link, name.split('_'))
        try:
            n[p[-2]].append(e)
        except KeyError:
            n[p[-2]] = [e]
    for ifo in nav:
        nav[ifo] = sorted(nav[ifo], cmp=navsort)
    return nav


# collect common noises
def gen_noises(root, ifos, budgets, prefix=''):
    noises = {}
    paths = [find_all(os.path.join(budgets, ifo)) for ifo in ifos]
    paths = [y for x in paths for y in x]  # flatten list of lists
    for path in sorted(paths, cmp=pathdatesort):
        basepath = os.path.splitext(path[len(root)+1:])[0]
        ifo, dof, date = budget.info_from_file(basepath)

        nb = budget.Budget.from_hdf5(path)
        xy = (nb.attrs['xunit'], nb.attrs['yunit'])
        noiseinfo = (basepath, xy)
        for label, trace in nb.traces.items():
            tags = trace.tags | set([label.lower()])
            for tag in tags:
                noises.setdefault((dof, tag), []).append(noiseinfo)

    def norm_xy(xy):
        # canonicalize x/yunit fields somewhat
        return (''.join(c for c in xy[0].lower().replace('rthz', 'hz')
                        if c.isalpha()),
                ''.join(c for c in xy[1].lower().replace('rthz', 'hz')
                        if c.isalpha()))

    noises = {key: infolist for key, infolist in noises.items()
              if (len(infolist) > 1
                  and len(set(norm_xy(xy) for bp, xy in infolist)) == 1)}

    noisenav = {(dof, tag): os.path.join(
        prefix, dof, 'Noise_{dof}_{tag}.html'.format(dof=dof, tag=tag)
        ) for dof, tag in noises}

    return noises, noisenav


# sorting functions for reverse chronological order
def pathsort(a, b):
    a = a.split('/')
    b = b.split('/')
    a = budget.info_from_file(a[-1])
    b = budget.info_from_file(b[-1])
    return reverse_chrono_sort(a, b)


def pathdatesort(a, b):
    a = a.split('/')
    b = b.split('/')
    a = budget.info_from_file(a[-1])
    b = budget.info_from_file(b[-1])
    return -cmp(a[2], b[2])


def navsort(a, b):
    a = a[2][1:]
    b = b[2][1:]
    return reverse_chrono_sort(a, b)


def reverse_chrono_sort(a, b):
    ifo_a, dof_a, date_a = a
    ifo_b, dof_b, date_b = b
    return cmp(4*cmp(ifo_a, ifo_b) + 2*cmp(dof_a, dof_b) - cmp(date_a, date_b),
               0)

##########


def main():
    PROG = 'nbweb'
    parser = argparse.ArgumentParser(prog=PROG)
    parser.add_argument('root', nargs='?', default=DEFAULT_ROOT,
                        help='web root [{}]'.format(DEFAULT_ROOT))
    parser.add_argument('-p', '--plot', action='store_true',
                        help='generate plots')
    parser.add_argument('-m', '--html', action='store_true',
                        help='generate html')
    parser.add_argument('-r', '--project', default='LIGO',
                        help='project name')
    parser.add_argument('-a', '--autorange', action='store_true',
                        help='auto-range axes')
    parser.add_argument('-b', '--backend', default='bokeh',
                        help='plotting backend')
    parser.add_argument('-u', '--url', default=DEFAULT_URL,
                        help='root url')
    parser.add_argument('-d', '--dataurl', default=DEFAULT_DATAURL,
                        help='data repo url')
    parser.add_argument('-s', '--summaryurl', default=DEFAULT_SUMMARYURL,
                        help='status summary url')
    # parser.add_argument('-f', '--force', action='store_true',
    #                     help='force re-generation')
    args = parser.parse_args()

    budgets = os.path.join(args.root, 'budgets')

    nav = gen_nav(budgets, prefix=args.url+'/budgets/')

    env = jinja2.Environment(
        loader=jinja2.PackageLoader('nbweb', 'templates'),
        )

    plot = importlib.import_module('nbweb.plot.' + args.backend + '_')

    ifos = sorted(dir for dir in os.listdir(budgets)
                  if os.path.isdir(os.path.join(budgets, dir))
                  and not dir.startswith('.'))

    noises, noisenav = gen_noises(args.root, ifos, budgets,
                                  prefix=args.url+'/noises/')

    # make budget plots and html
    latests = []
    for ifo in ifos:
        print(ifo)

        first = True
        for path in sorted(find_all(os.path.join(budgets, ifo)), cmp=pathsort):
            # print('  '+pb)
            # args.root + basepath + ext == pb
            basepath = os.path.splitext(path[len(args.root)+1:])[0]
            print('  '+basepath)

            ifo, dof, date = budget.info_from_file(basepath)
            print('    {} {}'.format(ifo, dof, date))

            if args.plot:
                nb = budget.Budget.from_hdf5(path, '/')
                if args.autorange:
                    xlim, ylim = util.autorange_budget(nb)
                    plot.write_budget_plot(nb, path, args.url, basepath,
                                           xlim=xlim, ylim=ylim)
                else:
                    plot.write_budget_plot(nb, path, args.url, basepath)

            if args.html:
                baseurl = os.path.join(args.url, basepath)
                print('    {}'.format(baseurl))

                gitcmd = 'cd {path} && git log --follow {file}'.format(
                    path=os.path.dirname(path), file=os.path.basename(path))
                gitlog = subprocess.check_output(gitcmd, shell=True)
                gitlog = jinja2.escape(gitlog)

                plot.write_budget_html(
                    env.get_template('budget.html'),
                    args.root,
                    args.url,
                    basepath,
                    project=args.project,
                    ifo=ifo,
                    dof=dof,
                    date=date,
                    dataexts=['.pdf', '.svg', '.hdf5'],
                    gitlog=gitlog,
                    dataurl=args.dataurl.format(ifo=ifo, dof=dof,
                                                project=args.project),
                    summaryurl=(args.summaryurl
                                + date.split('T')[0].replace('-', '')),
                    nav=nav,
                    noisenav=noisenav
                )

                if first:
                    latests.append(basepath)
                    ls = os.path.basename(basepath)+'.html'
                    lt = os.path.join(budgets, ifo, 'index.html')
                    print('  {} -> {}'.format(ls, lt))
                    try:
                        os.remove(lt)
                    except OSError:
                        pass
                    os.symlink(ls, lt)
                    first = False

    for dof, tag in noises:
        print(tag)

        noise_basepath = 'noises/{dof}/Noise_{dof}_{tag}'.format(dof=dof,
                                                                 tag=tag)
        if not os.path.exists(args.root+'/noises/'+dof):
            os.makedirs(args.root+'/noises/'+dof)
        if args.plot:
            noiselist = []
            for basepath, xy in noises[(dof, tag)]:
                nb = budget.Budget.from_hdf5(args.root+'/'+basepath+'.hdf5')
                noiselist += [(basepath, nb.freq, t.data)
                              for l, t in nb.traces.items()
                              if tag in (set([l.lower()]) | t.tags)]
            if args.autorange:
                xlim, ylim = util.autorange_noises(noiselist)
                plot.write_noise_plot(dof, tag, noiselist, args.url,
                                      os.path.join(args.root, noise_basepath),
                                      xlim=xlim, ylim=ylim,
                                      x_axis_label=xy[0], y_axis_label=xy[1])
            else:
                plot.write_noise_plot(dof, tag, noiselist, args.url,
                                      os.path.join(args.root, noise_basepath),
                                      x_axis_label=xy[0], y_axis_label=xy[1])

        if args.html:
            baseurl = os.path.join(args.url, noise_basepath)
            print('    {}'.format(baseurl))

            plot.write_noise_html(
                env.get_template('noise.html'),
                args.root,
                args.url,
                noise_basepath,
                dof,
                tag,
                project=args.project,
                dataexts=['.pdf', '.svg'],
                nav=nav,
                noisenav=noisenav
            )

    if args.html:
        print('index.html')
        plot.write_front_html(
            env.get_template('front.html'),
            os.path.join(args.root, 'index.html'),
            args.root,
            args.url,
            sorted(latests, cmp=pathsort),
            project=args.project,
            nav=nav,
            noisenav=noisenav
            )


if __name__ == '__main__':
    main()
