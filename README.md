[![pipeline status](https://git.ligo.org/NoiseBudget/nbweb/badges/master/pipeline.svg)](https://git.ligo.org/NoiseBudget/nbweb/commits/master)

# aLIGO Noise Budget Web

This repository hosts the code to generate plots and html pages for the [Noisebudget web space](https://ldas-jobs.ligo.caltech.edu/~noisebudget/).

## Contributing a noise budget
How to add a noise budget to the web archive:
1. Make a noise budget
1. Write the noise curves and metadata to an HDF5 file
1. Push the HDF5 file to the relevant NBdata repo: [H1](https://git.ligo.org/NoiseBudget/NBdata_H1), [L1](https://git.ligo.org/NoiseBudget/NBdata_L1)

The HDF5 file format is defined in [HDF5_SCHEMA.md](HDF5_SCHEMA.md). Files should be named as `NB_{IFO}_{DOF}_{UTCDATE}T{UTCTIME}Z.hdf5`.

***When pushing a new noise budget, please take a moment to write a useful commit message.*** This can be a comment on how the noise budget was constructed/altered — or a link to its alog entry.

Changes in either NBdata repo will trigger the CI to render updated web pages here: https://noisebudget.docs.ligo.org/nbweb/