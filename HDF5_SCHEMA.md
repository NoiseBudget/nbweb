# LIGO noise budget HDF5 schema
## Introduction
This file describes a schema for HDF5 storage of noise budgets.

HDF5 is a [hierarchical, structured data storage format][0].  Content is organized into a hierarchical folder-like structure, with two types of named objects:
- groups: holder of other objects (like folders)
- datasets: holder of data arrays (like files)

Objects can also have attributes as (almost) arbitrary key:value pairs.

Bindings are available for most platforms including [Python][1] and Matlab.

[0]: https://en.wikipedia.org/wiki/Hierarchical_Data_Format
[1]: http://www.h5py.org/

## Version history
- v1
  - first versioned schema release
- v2
  - hierarchical structure, tagging

## Schema
The following describes the noise budget schema.  Specific strings are enclosed in single quotes (`''`), and variables are described in brackets (`<>`).  Group objects are indicated by a closing `/` separator, data sets are indicated by a closing `:` followed by a specification of their length and type (e.g. `(N),float`), and attributes are specified in the `attrs[]` dictionary format.  Objects starting with a `*` are considered optional.

The following two root attributes are a string describing the schema, and an int schema version number:
- `/attrs['schema']` = `'LIGO Noise Budget'`
- `/attrs['schema_version']` = `2`

The following are the actual budget data set, including a top-level data set for the frequency array.  Contents of the `traces` group can be either trace datasets, or groups of trace datasets, etc. recursively:
- `/'frequency': (N),float`
- `/'traces'/`
  - `/*<trace_0>: (N),float`
  - `/*<trace_1>: (N),float`
  - ...
  - `/*<trace_N>/`
     - `/*<trace_Na>: (N),float`
     - `/*<trace_Nb>: (N),float`
     - ...

Each trace group, including the top-level `/'traces'/`, may contain the following metadata attributes:
- `/*attrs['name']` = \<noise budget title string\>
- `/*attrs['date']` = \<ISO-formatted string (e.g. `'2015-10-24T20:30:00Z'`)\>
- `/*attrs['parameters']` = \<YAML-encoded string of parameters\>

- `/*attrs['tags']` = \<tag list string\>
- `/*attrs['xunit']` = \<x axis label (default `'Frequency [Hz]'`)\>
- `/*attrs['yunit']` = \<y axis label (default `'Displacement [m/√Hz]'`)\>

Individual traces may also contain `'tags'` and `'parameters'` metadata.

Sum traces, and measured or other reference traces, may be included in a trace group.  If present, they should be tagged as `'sum'`, `'measured'`, or `'reference'`.
